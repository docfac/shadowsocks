#
# Dockerfile for shadowsocks-libev
#

FROM alpine:3.10 AS builder
LABEL maintainer="DocFac <docfac@protonmail.com>"

RUN set -ex \
 # Build environment setup
 && apk add --no-cache --virtual .build-deps \
      autoconf \
      automake \
      build-base \
      c-ares-dev \
      libev-dev \
      libtool \
      libsodium-dev \
      linux-headers \
      mbedtls-dev \
      pcre-dev \
      git \
 && git clone https://github.com/shadowsocks/shadowsocks-libev.git /tmp/shadowsocks-libev \
 && cd /tmp/shadowsocks-libev \
 && git submodule update --init --recursive \
 # Build & install
 && cd /tmp/shadowsocks-libev \
 && ./autogen.sh \
 && ./configure --prefix=/usr --disable-documentation \
 && make install

FROM alpine:3.10
LABEL maintainer="DocFac <docfac@protonmail.com>"

ENV SERVER_ADDR 0.0.0.0
ENV SERVER_PORT 8388
ENV PASSWORD=
ENV METHOD      aes-256-gcm
ENV TIMEOUT     300
ENV DNS_ADDRS   8.8.8.8,8.8.4.4
ENV ARGS=

COPY --from=builder /usr/bin/ss-* /usr/bin/

# Runtime dependencies setup
RUN apk add --no-cache \
     rng-tools \
     $(scanelf --needed --nobanner /usr/bin/ss-* \
     | awk '{ gsub(/,/, "\nso:", $2); print "so:" $2 }' \
     | sort -u)

USER nobody

HEALTHCHECK --interval=5s --timeout=3s --retries=12 \
 CMD ps | awk '{print $4}' | grep ss-server || exit 1

CMD exec ss-server \
      -s $SERVER_ADDR \
      -p $SERVER_PORT \
      -k ${PASSWORD:-$(hostname)} \
      -m $METHOD \
      -t $TIMEOUT \
      -d $DNS_ADDRS \
      -u \
      $ARGS
