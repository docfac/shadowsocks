# Shadowsocks-libev Docker Image

Base at [shadowsocks-libev](https://github.com/shadowsocks/shadowsocks-libev/tree/master/docker/alpine)

## Build image

```sh
$ docker build -t docfac/shadowsocks-libev:alpine .
```

## Start a container

```sh
$ docker run -p 8388:8388 -p 8388:8388/udp -d --restart always shadowsocks/shadowsocks-libev:alpine
```

## Use docker-compose to manage (optional)

It is very handy to use docker-compose to manage docker containers. You can download the binary at <https://github.com/docker/compose/releases>.

This is a sample `docker-compose.yml` file.

```yaml
version: "3.7"
services:
  shadowsocks:
    build: ./
    image: docfac/shadowsocks-libev:alpine
    container_name: shadowsocks-libev
    expose:
      - 8388
    ports:
      - "8388:8388/tcp"
      - "8388:8388/udp"
    environment:
      - METHOD=xchacha20-ietf-poly1305
      - PASSWORD=your-password
      - DNS_ADDRS=127.0.0.1,1.1.1.1,8.8.8.8
      - ARGS=-v
    restart: always
```

## TODO

[] commit to docker hub
